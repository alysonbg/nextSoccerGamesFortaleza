from .Team import Team
from bs4 import BeautifulSoup


class Ceara(Team):
    """ Class that represents the Ceara team. It extends the class team in order to implement its own getgame 
    method.
    """

    def getgame(self):
        self.driver.get(self.site)
        try:
            # Try to get the page html
            html = self.driver.page_source

            # Inicialize the beautifulsoup object
            self.soup = BeautifulSoup(html, "lxml")
        except:
            print("Unable to get the page html")

        timecasa = self.soup.find(class_="time casa").find("span")
        timevisitante = self.soup.find(class_="time visitante").find("span")
        datab = self.soup.find(class_="data").get_text()

        local, data, horario = datab.split('-')

        jogo = {"timecasa": timecasa.get_text(),
                "timevisitante": timevisitante.get_text(),
                "data": data,
                "horario": horario,
                "local": local}

        if jogo != self.nextgame:
            self.nextgame = jogo