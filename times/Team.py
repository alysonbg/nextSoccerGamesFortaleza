from selenium import webdriver
from bs4 import BeautifulSoup
import abc 


class Team():
    __metaclass__  = abc.ABCMeta
    """
    This class represents one of the soccer teams that are located on Fortaleza

    Args:
        :param site (string): This param is the website of the soccer team

    Example:
        team = Team("http://teamsite.com.br")
    """

    def __init__(self, site):
        self.site = site
        self.driver = None
        self.soup = None
        self.nextgame = None 
        
        try:
            # Create a new giSelenium browser with PhantomJs
            self.driver = webdriver.PhantomJS("library/phantomjs")
             
        except:
            print("Unable to Load the page with phantomjs")

    @abc.abstractmethod
    def getgame(self):
        """
        This method must be implemented in order to get the next game from the soccer Team. 
        It must load the webpage from the selenium driver and get the information using a BeautifulSoup
        object and save it on the database.

        Example:
            self.driver.get(self.site)
            
            # get the site's html
            html = self.driver.page_source

            # Inicialise a beatiful soup
            self.soup = BeautifulSoup(html, "lxml")
            
            # Put information on a dictionary

            game = {"house" : self.soup.find().get_text(), "visitor" : self.soup.find().get_text()}

            # Save the dictionary on the database

            self.db.saveGame(self.name, game)

        """
       



        
        

    




