from .Team import Team
from bs4 import BeautifulSoup


class Fortaleza(Team):
    """ This class represents the soccer team Fortaleza.
        It extends the class Team in order to implement the getgame method
        to get the information at the official team's web site.
    """
    
    def getgame(self):
        self.driver.get(self.site)
        try:
            # get the site's html
            html = self.driver.page_source

            # Inicialise a beatiful soup
            self.soup = BeautifulSoup(html, "lxml")
        except:
            print("Problem creating the BeautifulSoup object")

        datab = self.soup.find(class_="data").get_text()
        datab = datab.split(" ")

        data, horario = datab


        jogo = {"timecasa": self.soup.find(class_="time-left").get_text(),
                "timevisitante": self.soup.find(class_="time-right").get_text(),
                "data": data,
                "horario": horario,
                "local": self.soup.find(class_="local").get_text()}
        
        if jogo != self.nextgame:
            self.nextgame = jogo