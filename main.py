from times.Fortaleza import Fortaleza
from times.Ceara import Ceara
from pycnic.core import WSGI, Handler
import json


class JFortaleza(Handler):
    
    def get(self, name="Fortaleza"):
        # Creates a Fortaleza object in order to get the next game.
        fortaleza = Fortaleza("http://fortalezaec.net")
        # Get the next game and stores it on the variable forgame
        fortaleza.getgame()
        forgame = fortaleza.nextgame

        # Convert the game dictionary into json
        return json.dumps(forgame)


class JCeara(Handler):
    def get(self, name="Ceara"):
        # Creates a Ceara object in order to get the next game.
        ceara = Ceara("http://www.cearasc.com/home/")
        # Get the next game and stores it on the variable cegame
        ceara.getgame()
        cegame = ceara.nextgame

        # Convert the game dictionary into json
        return json.dumps(cegame)


class Root(Handler):
    def get(self, name="Root"):
        return {"status" : "OK"}


class app(WSGI):
    routes = [
        ("/fortaleza", JFortaleza()),
        ("/ceara", JCeara()),
        ("/", Root())
    ]

